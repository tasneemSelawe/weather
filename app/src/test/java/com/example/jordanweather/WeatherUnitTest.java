package com.example.jordanweather;

import com.example.jordanweather.dao.local.CityDataBase;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WeatherUnitTest {

    @Test
    public void validCityId() {
        String cityId = "250441";
        assertEquals(CityDataBase.getInstance().getCityWeather(Integer.parseInt(cityId)).getId(), cityId);
    }
}
