package com.example.jordanweather.data.db;

import com.example.jordanweather.common.MyApplication;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;

public class RealmDataBase {


    private static RealmDataBase instance;

    public static RealmDataBase getInstance() {
        if (instance == null) {
            instance = new RealmDataBase();
        }
        return instance;
    }

    // write to realm
    public <T extends RealmModel> void insert(T modelObject) {
        MyApplication.getInstance().getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(modelObject);
            }
        });
    }

    // read from realm
    public <T extends RealmModel> RealmResults<T> read(Class<T> modelObject) {
        return MyApplication.getInstance().getRealm().where(modelObject).findAll();
    }

    public <T extends RealmModel> T getFirst(Class<T> modelObject, String col, int value) {
        return MyApplication.getInstance().getRealm().where(modelObject).equalTo(col, value).findFirst();
    }

    // delete all from realm
    public void deleteAll(Class<? extends RealmModel> modelObject) {
        RealmResults realmResults = read(modelObject);
        if (realmResults != null && realmResults.size() > 0) {
            MyApplication.getInstance().getRealm().beginTransaction();
            realmResults.deleteAllFromRealm();
            MyApplication.getInstance().getRealm().commitTransaction();
        }
    }
}
