package com.example.jordanweather.constant;

public class AppConstant {
    public static final String BASE_URL = "https://api.openweathermap.org/";
    public static final String APP_ID = "9dc486e0382af86e3d3d7419a0e6a99d";
    public static final String getCityWeather = "data/2.5/weather";
    public static final String  AMMAN_CITY_ID = "250441";
    public static final String IRBID_CITY_ID = "248944";
    public static final String AQABA_CITY_ID = "250774";
}
