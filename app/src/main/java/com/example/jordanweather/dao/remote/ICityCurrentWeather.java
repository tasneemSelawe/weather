package com.example.jordanweather.dao.remote;

import com.example.jordanweather.constant.AppConstant;
import com.example.jordanweather.weatherdetails.model.CityWeatherModel;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ICityCurrentWeather {

    @GET(AppConstant.getCityWeather)
    Observable<Response<CityWeatherModel>> getCityWeather(@Query("id") String  id,
                                                          @Query("appid") String  appid);


}
