package com.example.jordanweather.dao.local;

import com.example.jordanweather.data.db.RealmDataBase;
import com.example.jordanweather.weatherdetails.model.CityWeatherModel;

public class CityDataBase {

    private static CityDataBase instance;


    public static CityDataBase getInstance() {
        if (instance == null) {
            instance = new CityDataBase();
        }
        return instance;
    }

    public void setCityWeather(CityWeatherModel cityWeather) {
        RealmDataBase.getInstance().insert(cityWeather);
    }

    public CityWeatherModel getCityWeather(int cityId) {
        return RealmDataBase.getInstance().getFirst(CityWeatherModel.class, "id", cityId);
    }

    public void deleteCityWeather()
    {
        RealmDataBase.getInstance().deleteAll(CityWeatherModel.class);
    }


}
