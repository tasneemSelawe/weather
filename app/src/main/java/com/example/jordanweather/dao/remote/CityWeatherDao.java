package com.example.jordanweather.dao.remote;

import com.example.jordanweather.data.network.RequestHelper;
import com.example.jordanweather.weatherdetails.model.CityWeatherModel;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class CityWeatherDao {
    public Map<String, String > params;

    public Observable<Response<CityWeatherModel>> get() {
        return requestServer().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<Response<CityWeatherModel>> requestServer() {
        return RequestHelper.getInstance().create(ICityCurrentWeather.class).
                getCityWeather(params.get("id"), params.get("appid"));
    }


    public CityWeatherDao setParams(Map<String, String > params) {
       this.params = params;
       return this;
    }

}
