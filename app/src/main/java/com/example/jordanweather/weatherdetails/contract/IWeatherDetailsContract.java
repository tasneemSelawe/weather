package com.example.jordanweather.weatherdetails.contract;

import com.example.jordanweather.weatherdetails.model.CityWeatherModel;

public interface IWeatherDetailsContract {
    public interface View {
        void setWeatherData(CityWeatherModel cityWeatherModel);


    }

    public interface presenter {
        void getWeatherData(String cityId);
    }
}

