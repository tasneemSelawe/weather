package com.example.jordanweather.weatherdetails.presenter;

import android.util.Log;

import com.example.jordanweather.constant.AppConstant;
import com.example.jordanweather.dao.local.CityDataBase;
import com.example.jordanweather.dao.remote.CityWeatherDao;
import com.example.jordanweather.weatherdetails.contract.IWeatherDetailsContract;
import com.example.jordanweather.weatherdetails.model.CityWeatherModel;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Consumer;
import retrofit2.Response;

public class WeatherPresenter implements IWeatherDetailsContract.presenter {
    private IWeatherDetailsContract.View view;

    public WeatherPresenter(IWeatherDetailsContract.View view) {
        this.view = view;
    }

    @Override
    public void getWeatherData(String cityId) {
        if (CityDataBase.getInstance().getCityWeather(Integer.parseInt(cityId)) != null &&
                CityDataBase.getInstance().getCityWeather(Integer.parseInt(cityId)).getId() == Integer.parseInt(cityId)) {
            view.setWeatherData(CityDataBase.getInstance().getCityWeather(Integer.parseInt(cityId)));
            Log.d("found", "true");
        } else {
            Log.d("found", "false");
            CityWeatherDao cityWeatherDao = new CityWeatherDao();
            Map<String, String> params = new HashMap<>();
            params.put("id", cityId);
            params.put("appid", AppConstant.APP_ID);
            cityWeatherDao.setParams(params).get().subscribe(new Consumer<Response<CityWeatherModel>>() {
                @Override
                public void accept(Response<CityWeatherModel> cityWeatherModelResponse) throws Exception {
                    Log.d("code", cityWeatherModelResponse.code() + "");
                    view.setWeatherData(cityWeatherModelResponse.body());
                    CityDataBase.getInstance().setCityWeather(cityWeatherModelResponse.body());
                }
            }, throwable -> {
                Log.d("code", throwable.getMessage());

            });
        }

    }
}
