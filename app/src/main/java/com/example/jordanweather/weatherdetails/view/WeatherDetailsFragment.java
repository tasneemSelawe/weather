package com.example.jordanweather.weatherdetails.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jordanweather.R;
import com.example.jordanweather.weatherdetails.contract.IWeatherDetailsContract;
import com.example.jordanweather.weatherdetails.model.CityWeatherModel;
import com.example.jordanweather.weatherdetails.presenter.WeatherPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class WeatherDetailsFragment extends Fragment implements IWeatherDetailsContract.View {

    Unbinder unbinder;
    @BindView(R.id.tv_city_name)
    AppCompatTextView tvCityName;
    @BindView(R.id.tv_temp)
    AppCompatTextView tvTemp;
    @BindView(R.id.tv_description)
    AppCompatTextView tvDescription;
    @BindView(R.id.tv_wind_speed)
    AppCompatTextView tvWindSpeed;
    @BindView(R.id.tv_pressure_speed)
    AppCompatTextView tvPressureSpeed;
    @BindView(R.id.tv_humidity_speed)
    AppCompatTextView tvHumiditySpeed;
    @BindView(R.id.iv_weather)
    AppCompatImageView ivWeather;
    @BindView(R.id.iv_weather_status)
    AppCompatImageView ivWeatherStatus;
    private View view;
    private String cityId;


    public WeatherDetailsFragment() {
        // Required empty public constructor
    }

    public static WeatherDetailsFragment newInstance(String cityId) {

        Bundle args = new Bundle();
        args.putString("CITY_ID", cityId);
        WeatherDetailsFragment fragment = new WeatherDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cityId = getArguments().getString("CITY_ID");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_weather_details, container, false);
        Log.d("city_id", cityId);
        unbinder = ButterKnife.bind(this, view);
        WeatherPresenter weatherPresenter = new WeatherPresenter(this);
        weatherPresenter.getWeatherData(cityId);
        return view;
    }

    @Override
    public void setWeatherData(CityWeatherModel cityWeatherModel) {
        tvCityName.setText(cityWeatherModel.getName());
        tvDescription.setText(cityWeatherModel.getWeather().get(0).getDescription());
        tvHumiditySpeed.setText(cityWeatherModel.getMain().getHumidity() + "");
        tvPressureSpeed.setText(cityWeatherModel.getMain().getPressure() + "");
        tvWindSpeed.setText(cityWeatherModel.getWind().getSpeed() + "");
        tvTemp.setText(cityWeatherModel.getMain().getTemp() + "");
        if (cityWeatherModel.getWeather().get(0).getDescription().contains("haze")||
                cityWeatherModel.getWeather().get(0).getDescription().contains("mist")) {
            ivWeather.setImageResource(R.drawable.hazy);
            ivWeatherStatus.setImageResource(R.drawable.ic_haze);
        } else if (cityWeatherModel.getWeather().get(0).getDescription().contains("clear")) {
            ivWeather.setImageResource(R.drawable.sunny);
            ivWeatherStatus.setImageResource(R.drawable.ic_sun);
        } else  {
            ivWeather.setImageResource(R.drawable.cloudy);
            ivWeatherStatus.setImageResource(R.drawable.ic_cloud);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
