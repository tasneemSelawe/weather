package com.example.jordanweather.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.jordanweather.R;
import com.example.jordanweather.main.view.MainActivity;

public class SplashScreenActivity extends AppCompatActivity {
    private Intent myintent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        myintent = new Intent(this, MainActivity.class);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(myintent);
                finish();
            }
        }, 2000);
    }

}
