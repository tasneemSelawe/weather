package com.example.jordanweather.main.contract;

import com.example.jordanweather.weatherdetails.view.WeatherDetailsFragment;

public interface IMainContract {

    public interface View {
        void setViewPagerAdapter();
    }

    public interface Presenter {
        void getViewPagerAdapter();
        WeatherDetailsFragment setPage();

    }
}
