package com.example.jordanweather.main.presenter;

import com.example.jordanweather.main.contract.IMainContract;
import com.example.jordanweather.main.model.CityIdModel;
import com.example.jordanweather.weatherdetails.view.WeatherDetailsFragment;

public class MainPresenter implements IMainContract.Presenter {

    IMainContract.View view;
    CityIdModel cityIdModel;

    public MainPresenter(IMainContract.View view, CityIdModel cityIdModel) {
        this.view = view;
        this.cityIdModel = cityIdModel;
    }

    @Override
    public void getViewPagerAdapter() {
        view.setViewPagerAdapter();
    }

    @Override
    public WeatherDetailsFragment setPage() {
        return WeatherDetailsFragment.newInstance(cityIdModel.getCityId());
    }
}
