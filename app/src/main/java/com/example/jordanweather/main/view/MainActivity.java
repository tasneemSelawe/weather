package com.example.jordanweather.main.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.jordanweather.R;
import com.example.jordanweather.constant.AppConstant;
import com.example.jordanweather.dao.local.CityDataBase;
import com.example.jordanweather.main.contract.IMainContract;
import com.example.jordanweather.main.model.CityIdModel;
import com.example.jordanweather.main.presenter.MainPresenter;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMainContract.View {
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator)
    WormDotsIndicator dotsIndicator;
    private ViewPagerAdapter adapter;
    private MainPresenter mainPresenter;
    private CityIdModel cityIdModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        cityIdModel = new CityIdModel();
        mainPresenter = new MainPresenter(this,cityIdModel);
        mainPresenter.getViewPagerAdapter();



    }

    @Override
    public void setViewPagerAdapter() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        dotsIndicator.setViewPager(viewPager);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    cityIdModel.setCityId(AppConstant.AMMAN_CITY_ID);
                    return mainPresenter.setPage();
                   // return WeatherDetailsFragment.newInstance(AppConstant.AMMAN_CITY_ID);
                case 1:
                    cityIdModel.setCityId(AppConstant.IRBID_CITY_ID);
                    return mainPresenter.setPage();
                   // return WeatherDetailsFragment.newInstance(AppConstant.IRBID_CITY_ID);
                case 2:
                    cityIdModel.setCityId(AppConstant.AQABA_CITY_ID);
                    return mainPresenter.setPage();
                 //   return WeatherDetailsFragment.newInstance(AppConstant.AQABA_CITY_ID);
                default:
                    return null;
            }

        }

    }

    @Override
    protected void onDestroy() {
        CityDataBase.getInstance().deleteCityWeather();
     //   MyApplication.getInstance().getRealm().close();
        super.onDestroy();

    }
}
