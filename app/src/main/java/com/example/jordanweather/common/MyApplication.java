package com.example.jordanweather.common;

import android.app.Application;

import com.example.jordanweather.dao.local.CityDataBase;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends Application {


    private static MyApplication instance;
    private Realm realm;

    public static synchronized MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        CityDataBase.getInstance().deleteCityWeather();
        realm.close();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(this);

    }

    public synchronized Realm getRealm() {
        if (realm == null) {
            RealmConfiguration configuration = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            realm = Realm.getInstance(configuration);
        }
        return realm;
    }


}
